# Schildermeister

JSON Schemata und Links zu Werkzeugen für die Bearbeitung von Wegweisungs- und Beschilderungssystemen im Bereich Wandern und Radfahren.

## Motivation

Durch die Bearbeitung von Planungs- und Wegweisungsprojekten, insbesondere die informationstechnische Begleitung, enstand sehr schnell der Wunsch nach standardisierten Datentausch um die aufwändigen Medien- und Datenformatbrüche zu eliminieren, die notwendigen Informationen in den einzelnen Arbeitsschritten zu minimieren und trotzdem ein anschaulich korrekt layoutetes Ergebnis zu liefern.

## Ziele

1. Beschreibung der Wegweiser und Sandorte als JSON Datenobjekt,

2. ...um ein sriptgesteuertes Generieren von Grafiken und druckfertigen Layouts zu ermöglichen.

3. ...einheitliche Datentransferstrukturen

4. ...Bereitstellung oder Verlinkung aller notwendigen Symbole, Logos und Bezeichnungen der Wege.

5. ...Verlinkung der Tools die diese Daten Erzeugen und Verarbeiten können.

Das Ganze als Open Source und frei nutzbare Ressourcen, da wesentliche Teile der Arbeit in dem Bereich auch in Zukunft ehrenamtlich über Wanderverbände und lokale Vereine erfolgen wird.

## Verzeichnisstruktur

Die Verzeichnisstruktur ist nach der Gebietshierarchie gegliedert, für die gemeinsame Standards abgestimmt wurden.
Z.B. für Thüringen:

* [./world/europe/de/de-th/README.md](./world/europe/de/de-th/README.md)

## Tools

* [sign](https://gitlab.com/wegemeister/sign) Linux commandline tool. Erzeug aus JSON zu diesem Schema svg Dateien, siehe auch [wegemeister.de](https:://wegemeister.de/tools/)

## Autor

(c) 2019 Ervin Peters (<coder@wegemeister.de>, <https://wegemeister.de>)  
