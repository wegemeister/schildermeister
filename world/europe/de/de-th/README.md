# europe/de/de-th/Wanderwegweisertafel

JSON Schemata zur Beschreibung einer [Wanderwegweisertafel](./Wanderwegweisertafel.json), nach dem ["Praxisleitfaden - Touristische Wanderwegekonzeption. Thüringen 2025"](https://www.thueringen.de/de/publikationen/pic/pubdownload1744.pdf), des Ministerium für Wirtschaft, Wissenschaft und Digitale Gesellschaft (2017).

Fortschreibung unter Koordination der [Thüringer Tourismus GmbH](https://thueringen.tourismusnetzwerk.info/inhalte/produktentwicklung/wandern/) 


## Autor

(c) 2019 Ervin Peters (<coder@wegemeister.de>, <https://wegemeister.de>)  
